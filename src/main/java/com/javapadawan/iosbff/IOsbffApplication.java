package com.javapadawan.iosbff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class IOsbffApplication {

	public static void main(String[] args) {
		SpringApplication.run(IOsbffApplication.class, args);
	}

}
