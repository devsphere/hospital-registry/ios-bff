package com.javapadawan.iosbff.entity.domain;

public class RegistryUser {
    private String token;
    private String role;
    Patient person;

    public RegistryUser() {
    }

    public RegistryUser(String role, Patient person, String token) {
        this.role = role;
        this.person = person;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Object getPerson() {
        return person;
    }

    public void setPerson(Patient person) {
        this.person = person;
    }
}
