package com.javapadawan.iosbff.service;

import com.javapadawan.iosbff.entity.domain.Doctor;
import com.javapadawan.iosbff.entity.domain.Patient;
import com.javapadawan.iosbff.entity.payload.DoctorDto;
import com.javapadawan.iosbff.entity.payload.ios.DoctorDtoIOS;
import com.javapadawan.iosbff.entity.payload.ReferralDto;
import com.javapadawan.iosbff.entity.payload.ios.ReferralDtoIOS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DomainService {
    private final WebClient webClient;

    @Autowired
    public DomainService(WebClient webClient) {
        this.webClient = webClient;
    }

    public Doctor[] getDoctorsBySample(DoctorDtoIOS dtoIOS) {
        DoctorDto doctorDto = new DoctorDto();
        doctorDto.setName(dtoIOS.getName());
        doctorDto.setLastname(dtoIOS.getLastname());
        doctorDto.setSpecialization(dtoIOS.getSpecialization());
        return webClient.requestDoctorsBySample(doctorDto);
    }

    public Doctor[] getAllDoctors() {
        return webClient.requestAllDoctors();
    }

    public Object[] getReferralsOnPatient(String email) {
        return webClient.requestReferralsOnPatientEmail(email);
    }

    public void saveReferral(ReferralDtoIOS dtoIOS) {
        ReferralDto dto = convertIOSDto(dtoIOS);
        webClient.addReferral(dto);
    }

    public void deleteReferral(ReferralDtoIOS dtoIOS) {
        ReferralDto dto = convertIOSDto(dtoIOS);
        webClient.deleteReferral(dto);
    }

    public void updateAuthHeader(String authHeader) {
        webClient.updateAuthHeader(authHeader);
    }

    private ReferralDto convertIOSDto(ReferralDtoIOS dtoIOS) {
        Patient patient = webClient.requestPatientFromDtoById(dtoIOS);
        Doctor doctor = webClient.requestDoctorFromDtoById(dtoIOS);
        ReferralDto dto = new ReferralDto();
        dto.setDoctorEmail(doctor.getEmail());
        dto.setPatientEmail(patient.getEmail());
        dto.setDate(dtoIOS.getAppointmentDate());
        dto.setTime(dtoIOS.getAppointmentTime());
        return dto;
    }
}
