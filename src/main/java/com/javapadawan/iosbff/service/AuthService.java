package com.javapadawan.iosbff.service;

import com.javapadawan.iosbff.entity.domain.Credential;
import com.javapadawan.iosbff.entity.domain.Patient;
import com.javapadawan.iosbff.entity.payload.AuthDto;
import com.javapadawan.iosbff.entity.domain.RegistryUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final WebClient webClient;

    @Autowired
    public AuthService(WebClient requester) {
        this.webClient = requester;
    }

    public ResponseEntity<RegistryUser> authenticateCredentials(Credential credential) {
        ResponseEntity<AuthDto> response = webClient.authenticate(credential);
        AuthDto dto = response.getBody();
        HttpHeaders headers = response.getHeaders();
        Patient patient = webClient.requestUserData(dto);
        String role = dto.getRole();
        String token = headers.getFirst("Authorization");
        RegistryUser user = new RegistryUser(role, patient, token);
        return new ResponseEntity<>(
                user,
                headers,
                HttpStatus.OK
        );
    }
}
