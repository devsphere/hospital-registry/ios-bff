package com.javapadawan.iosbff.service;

import com.javapadawan.iosbff.entity.domain.Credential;
import com.javapadawan.iosbff.entity.domain.Doctor;
import com.javapadawan.iosbff.entity.domain.Patient;
import com.javapadawan.iosbff.entity.payload.*;
import com.javapadawan.iosbff.entity.payload.ios.ReferralDtoIOS;
import com.javapadawan.iosbff.exception.DomainException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
public class WebClient {
    private final RestTemplate rest;
    private final String authServerAddress;
    private final String domainServerAddress;
    private HttpHeaders tokenHeader;

    @Autowired
    public WebClient(@Value("${app.authentication-server.url}") String authServerUrl,
                     @Value("${app.authentication-server.port}") String authServerPort,
                     @Value("${app.domain-server.url}") String domainServerUrl,
                     @Value("${app.domain-server.port}") String domainServerPort) {
        this.rest = new RestTemplate();
        this.domainServerAddress = domainServerUrl + domainServerPort;
        this.authServerAddress = authServerUrl + authServerPort;
        tokenHeader = new HttpHeaders();
        tokenHeader.add("Authorization", "");
    }

    //auth methods
    public void updateAuthHeader(String headerValue) {
        tokenHeader.set("Authorization", headerValue);
    }

    public ResponseEntity<AuthDto> authenticate(Credential credential) {
        try {
            return rest.exchange(
                    authServerAddress + "/authentication/credentials",
                    HttpMethod.POST,
                    new HttpEntity<>(credential),
                    AuthDto.class
            );
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromAuthDtoException(ex));
        }
    }

    public Patient requestUserData(AuthDto dto) {
        ResponseEntity<Patient> response = rest.exchange(
                domainServerAddress + "/authentication/byToken",
                HttpMethod.POST,
                new HttpEntity<>(dto, tokenHeader),
                Patient.class
        );
        return response.getBody();
    }

    //logic methods
    public Doctor[] requestAllDoctors() {
        ResponseEntity<Doctor[]> response = rest.exchange(
                domainServerAddress + "/public/staff/doctors",
                HttpMethod.GET,
                new HttpEntity<>(tokenHeader),
                Doctor[].class
        );
        return response.getBody();
    }

    public Object[] requestReferralsOnPatientEmail(String email) {
        ResponseEntity<Object[]> response = rest.exchange(
                domainServerAddress + "/logic/referral/onPatient/" + email,
                HttpMethod.GET,
                new HttpEntity<>(tokenHeader),
                Object[].class
        );
        return response.getBody();
    }

    public Doctor[] requestDoctorsBySample(DoctorDto dto) {
        ResponseEntity<Doctor[]> response = rest.exchange(
                domainServerAddress + "/logic/doctor/bySample",
                HttpMethod.POST,
                new HttpEntity<>(dto, tokenHeader),
                Doctor[].class
        );
        return response.getBody();
    }

    public Patient requestPatientFromDtoById(ReferralDtoIOS dto) {
        try{
            ResponseEntity<Patient> patient =  rest.exchange(
                domainServerAddress + "/logic/patient/getById/" + dto.getPersonId(),
                HttpMethod.GET,
                new HttpEntity<>(tokenHeader),
                Patient.class
            );
            return patient.getBody();
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromStringException(ex));
        }
    }

    public Doctor requestDoctorFromDtoById(ReferralDtoIOS dto) {
        try{
            ResponseEntity<Doctor> patient =  rest.exchange(
                    domainServerAddress + "/logic/doctor/getById/" + dto.getDoctorId(),
                    HttpMethod.GET,
                    new HttpEntity<>(tokenHeader),
                    Doctor.class
            );
            return patient.getBody();
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromStringException(ex));
        }
    }

    public void addReferral(ReferralDto dto) {
        try {
            rest.exchange(
                    domainServerAddress + "/logic/referral/save",
                    HttpMethod.POST,
                    new HttpEntity<>(dto, tokenHeader),
                    Void.class
            );
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromStringException(ex));
        }
    }

    public void deleteReferral(ReferralDto dto) {
        rest.exchange(
                domainServerAddress + "/logic/referral/delete",
                HttpMethod.DELETE,
                new HttpEntity<>(dto, tokenHeader),
                Void.class
        );
    }

    //private methods
    private String getMessageFromAuthDtoException(HttpStatusCodeException ex) {
        try {
            return ex.getResponseBodyAs(AuthDto.class).getMessage();
        } catch (NullPointerException npe) {
            return "Internal error occurred";
        }
    }

    private String getMessageFromStringException(HttpStatusCodeException ex) {
        return ex.getResponseBodyAs(String.class);
    }
}
