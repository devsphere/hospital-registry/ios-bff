package com.javapadawan.iosbff.controller;

import com.javapadawan.iosbff.entity.domain.Credential;
import com.javapadawan.iosbff.entity.domain.RegistryUser;
import com.javapadawan.iosbff.exception.DomainException;
import com.javapadawan.iosbff.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/ios/ui/security")
public class AuthController {
    private final AuthService service;

    @Autowired
    public AuthController(AuthService service) {
        this.service = service;
    }

    @PostMapping("/authentication")
    public ResponseEntity<RegistryUser> authenticate(@RequestBody Credential credential) {
        try {
            return service.authenticateCredentials(credential);
        } catch (DomainException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }
}
