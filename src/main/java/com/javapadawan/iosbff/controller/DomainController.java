package com.javapadawan.iosbff.controller;

import com.javapadawan.iosbff.entity.domain.Doctor;
import com.javapadawan.iosbff.entity.payload.ios.DoctorDtoIOS;
import com.javapadawan.iosbff.entity.payload.ios.ReferralDtoIOS;
import com.javapadawan.iosbff.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/ios/ui/domain")
public class DomainController {
    private final DomainService service;

    @Autowired
    public DomainController(DomainService service) {
        this.service = service;
    }

    @PostMapping("/doctor/bySample")
    public Doctor[] getDoctorsBySample(@RequestBody DoctorDtoIOS dto,
                                           @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        return service.getDoctorsBySample(dto);
    }

    @GetMapping("/doctor/all")
    public Doctor[] getAllDoctors() {
        return service.getAllDoctors();
    }

    @GetMapping("/referral/onPatient/{email}")
    public Object[] getReferralsOnPatient(@PathVariable String email,
                                              @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        return service.getReferralsOnPatient(email);
    }

    @PostMapping("/referral/save")
    public void saveReferral(@RequestBody ReferralDtoIOS dtoIOS,
                             @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            service.saveReferral(dtoIOS);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/referral/cancel")
    public void deleteReferral(@RequestBody ReferralDtoIOS dtoIOS,
                             @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            service.deleteReferral(dtoIOS);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
