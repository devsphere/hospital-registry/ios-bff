package com.javapadawan.iosbff.exception;

public class DomainException extends RuntimeException {
    public DomainException(String message) {
        super(message);
    }
}
